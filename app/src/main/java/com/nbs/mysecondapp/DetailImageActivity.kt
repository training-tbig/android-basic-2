package com.nbs.mysecondapp

import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.github.chrisbanes.photoview.PhotoView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_detail_image.*

class DetailImageActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_image)

        supportActionBar?.hide()

        val imageUrl = intent.getStringExtra(EXTRA_IMAGE_URL)
        Picasso.get()
            .load(imageUrl)
            .into(imgDetailPhoto)
    }

    companion object{
        const val EXTRA_IMAGE_URL = "IMAGE_URL"

        fun start(context: Context, imageUrl: String){
            val intent = Intent(context, DetailImageActivity::class.java)
            intent.putExtra(EXTRA_IMAGE_URL, imageUrl)
            context.startActivity(intent)
        }
    }
}
