package com.nbs.mysecondapp

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_simple_list.*

class SimpleListActivity : AppCompatActivity(),
    SimpleAdapter.OnItemClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_simple_list)

        //set the data
        val listMovie = mutableListOf(
            "Jurassic Park", "Jurassic World",
            "Starwars", "Man in Black",
            "Maju kena mundur kena",
            "Tertawalah sebelum tertawa itu dilarang",
            "Sama-sama Senang")

        //configure the recyclerview
        rvMovie.setHasFixedSize(true)
        rvMovie.layoutManager = LinearLayoutManager(this)
        rvMovie
            .addItemDecoration(DividerItemDecoration(this,
                DividerItemDecoration.VERTICAL))

        //create an adapter instance
        val adapter = SimpleAdapter(this)
        adapter.itemList.addAll(listMovie)

        //set adapter to RecyclerView
        rvMovie.adapter = adapter
    }

    override fun onItemClicked(item: String) {
        Toast
            .makeText(this, "$item clicked", Toast.LENGTH_LONG)
            .show()

        startActivity(Intent(this,
            MovieGalleryActivity::class.java))
    }

}
