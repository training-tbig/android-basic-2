package com.nbs.mysecondapp

interface OnIncreaseNumberListener {
    fun onNumberIncreased()
}