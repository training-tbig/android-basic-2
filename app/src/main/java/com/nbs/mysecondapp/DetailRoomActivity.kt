package com.nbs.mysecondapp

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_detail_room.*

class DetailRoomActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_room)

        supportActionBar?.apply {
            title = "Marriot Hotel"
            subtitle = "Surabaya, Indonesia"
            setDisplayHomeAsUpEnabled(true)
        }

        Picasso.get()
            .load("https://media-cdn.tripadvisor.com/media/photo-m/1280/14/8d/28/ed/chairman-suite-bedroom.jpg")
            .into(imgRoom)

        Picasso.get()
            .load("https://i1.wp.com/metro.co.uk/wp-content/uploads/2019/07/GettyImages-1163201466.jpg?quality=90&strip=all&zoom=1&resize=644%2C429&ssl=1")
            .into(imgHost)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_detail, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when(item?.itemId){
            R.id.action_share ->
                Toast.makeText(this, "Share clicked",
                    Toast.LENGTH_LONG).show()

            android.R.id.home -> finish()

            R.id.action_save ->
                Toast.makeText(this, "Save clicked",
                    Toast.LENGTH_LONG).show()
        }
        return super.onOptionsItemSelected(item)
    }
}
