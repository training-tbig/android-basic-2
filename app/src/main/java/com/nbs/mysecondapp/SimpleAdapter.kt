package com.nbs.mysecondapp

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.item_list.view.*

class SimpleAdapter(val onItemClickListener:
                    OnItemClickListener):
    RecyclerView.Adapter<SimpleAdapter.Viewholder>() {

    val itemList = mutableListOf<String>()

    //this method is to inflate the itemView layout in xml
    // and convert
    //it into itemView object
    override fun onCreateViewHolder(viewGroup: ViewGroup, position: Int): Viewholder {
        val itemView = LayoutInflater
            .from(viewGroup.context)
            .inflate(R.layout.item_list, viewGroup,
                false)
        return Viewholder(itemView)
    }

    //this method is to get total numbers of
    //items will be iterated
    override fun getItemCount() = itemList.size

    //this method is to bind the data into
    //list item object (ViewHolder)
    override fun onBindViewHolder(viewholder: Viewholder, position: Int) {
        viewholder.bind(itemList[position])
    }

    //Viewholder = item object manipulator
    inner class Viewholder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {

        //bind data to item view
        fun bind(item: String){
            itemView.tvItem.text = item

            itemView.itemList.setOnClickListener {
                onItemClickListener.onItemClicked(item)
            }
        }
    }

    //Callback action listener
    interface OnItemClickListener{
        fun onItemClicked(item: String)
    }
}