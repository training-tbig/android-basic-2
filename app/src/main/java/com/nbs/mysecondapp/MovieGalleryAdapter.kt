package com.nbs.mysecondapp

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_gallery.view.*

class MovieGalleryAdapter(val onMovieGalleryItemClicklistener:
                          OnMovieGalleryItemClicklistener):
    RecyclerView
    .Adapter<MovieGalleryAdapter.MovieGalleryViewholder>() {

    val movieImageUrls = mutableListOf<String>()

    var onClick: ((String) -> Unit) ?= null

    fun setOnItemClickListener(func: (String) -> Unit){
        this.onClick = func
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, position: Int): MovieGalleryViewholder {
        val itemView = LayoutInflater
            .from(viewGroup.context)
            .inflate(R.layout.item_gallery, viewGroup,
                false)
        return MovieGalleryViewholder(itemView)
    }

    override fun getItemCount() = movieImageUrls.size

    override fun onBindViewHolder(viewholder: MovieGalleryViewholder, position: Int) {
        viewholder.bind(movieImageUrls[position])
    }

    inner class MovieGalleryViewholder(itemView: View) :
        RecyclerView.ViewHolder(itemView){

        fun bind(imageUrl: String){
            Picasso.get()
                .load(imageUrl)
                .into(itemView.imgGallery)

            itemView.imgGallery.setOnClickListener {
//                onMovieGalleryItemClicklistener
//                    .onMovieGalleryItemClicked(imageUrl)
                onClick?.invoke(imageUrl)
            }
        }
    }

    interface OnMovieGalleryItemClicklistener{
        fun onMovieGalleryItemClicked(imageUrl: String)
    }
}