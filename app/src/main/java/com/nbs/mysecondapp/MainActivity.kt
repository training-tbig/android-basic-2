package com.nbs.mysecondapp

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast

class MainActivity : AppCompatActivity(), OnIncreaseNumberListener {

    var counter: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val bundle = Bundle()
        bundle.putString("name", "Sidiq Permana")

        val homeFragment = HomeFragment()
        homeFragment.onIncreaseNumberListener = this
        homeFragment.arguments = bundle

        supportFragmentManager.beginTransaction()
            .replace(R.id.containerHome, homeFragment,
                HomeFragment::class.java.simpleName)
            .commit()
    }

    override fun onNumberIncreased() {
        counter += 1

        Toast.makeText(this,
            "Dipanggil dari fragment, counter $counter", Toast.LENGTH_LONG)
            .show()
    }
}
