package com.nbs.mysecondapp


import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_home.*

class HomeFragment : Fragment() {

    var onIncreaseNumberListener:
            OnIncreaseNumberListener? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_home, container, false)

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        if (arguments != null){
            val name = arguments?.getString("name")
            tvHello.text = "Hai $name"
        }

        btnProfile.setOnClickListener {
            val intent = Intent(context,
                ProfileActivity::class.java)
            startActivity(intent)
        }

        btnUpdateCounter.setOnClickListener {
            onIncreaseNumberListener?.onNumberIncreased()
        }

        Picasso.get().load("https://www.partyrama.co.uk/wp-content/uploads/2017/06/minions-kevin-lifesize-cardboard-cutout-180cms-product-image.jpg")
            .into(imgMinions)
    }
}
