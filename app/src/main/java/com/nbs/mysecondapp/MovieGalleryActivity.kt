package com.nbs.mysecondapp

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import kotlinx.android.synthetic.main.activity_movie_gallery.*
import kotlinx.android.synthetic.main.activity_simple_list.*

class MovieGalleryActivity : AppCompatActivity(),
    MovieGalleryAdapter.OnMovieGalleryItemClicklistener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_movie_gallery)

        val imageUrls = mutableListOf(
            "https://cdn2.tstatic.net/tribunnews/foto/bank/images/warkop-dki_20170923_154556.jpg",
            "https://cdn.brilio.net/community/2018/03/06/7971/daftar-lengkap-34-film-warkop-dki-030621.jpg",
            "https://cdn-image.hipwee.com/wp-content/uploads/2016/09/hipwee-Ada-Penampakan-Almarhum-Kasino-di-Lokasi-Syuting-Warkop-DKI-Reborn.jpg",
            "https://cdn0-production-images-kly.akamaized.net/fgygFmEMHtOMGo7hRSnfbFULFPM=/680x383/smart/filters:quality(75):strip_icc():format(jpeg)/kly-media-production/medias/1345984/original/059393400_1473912698-14072864_1111575752261128_447126581_n.jpg",
            "https://cdns.klimg.com/merdeka.com/i/w/news/2016/01/05/646336/670x335/aksi-dono-warkop-dki-hadang-tentara-di-peristiwa-mei-98.jpg",
            "https://www.warkopdki.org/wp-content/uploads/2018/02/Poster-Film-Warkop-Mana-Tahan-2-630x380.jpg")

        rvGallery.setHasFixedSize(true)
        rvGallery.layoutManager = GridLayoutManager(this,
            3)

        val movieGalleryAdapter =
            MovieGalleryAdapter(this)
        movieGalleryAdapter.movieImageUrls.addAll(imageUrls)
        movieGalleryAdapter.setOnItemClickListener{
            //called from lambda function
            DetailImageActivity.start(this, it)
        }
        rvGallery.adapter = movieGalleryAdapter
    }

    override fun onMovieGalleryItemClicked(imageUrl: String) {
        //called from interface
        DetailImageActivity.start(this, imageUrl)
    }
}
